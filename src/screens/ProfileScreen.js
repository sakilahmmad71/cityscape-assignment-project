import ProfileComponent from '../components/profile/ProfileComponent';
import useLocalStorage from '../hooks/useLocalStorage';

const ProfileScreen = () => {
  const [userInformation] = useLocalStorage('supabase.auth.token');

  return (
    <div>
      <ProfileComponent profile={userInformation?.currentSession?.user} />
    </div>
  );
};

export default ProfileScreen;
