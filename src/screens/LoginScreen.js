import { useState } from 'react';
import LoginComponent from '../components/authentication/LoginComponent';
import { SuccessNotification } from '../components/common/ToastNotification';
import { supabase } from '../config/supabaseClient';

const LoginScreen = () => {
  const [requestStatus, setRequestStatus] = useState({ loading: false, error: '', success: false });

  const handleLoginUser = async (data, reset) => {
    const { email, password } = data;

    try {
      setRequestStatus((prevState) => ({ ...prevState, loading: true, error: '' }));
      const { user, error } = await supabase.auth.signIn({ email, password });

      if (user) {
        setRequestStatus((prevState) => ({ ...prevState, loading: false, success: true }));
        SuccessNotification('Login successfully done!');
        if (typeof reset === 'function') {
          reset();
          window.location.reload();
        }
      } else {
        setRequestStatus((prevState) => ({
          ...prevState,
          loading: false,
          error: error?.message
        }));
      }
    } catch (error) {
      setRequestStatus((prevState) => ({ ...prevState, loading: false, error: error?.message }));
    }
  };

  return (
    <div>
      <LoginComponent
        handleLoginUser={handleLoginUser}
        loading={requestStatus?.loading}
        success={requestStatus?.success}
        error={requestStatus?.error}
      />
    </div>
  );
};

export default LoginScreen;
