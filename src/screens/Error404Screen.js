const Error404Screen = () => (
  <>
    <div
      className="container-fluid"
      style={{ background: '#fff', height: '100vh', width: '100vw' }}>
      <div
        style={{
          left: '50%',
          position: 'absolute',
          top: '50%',
          msTransform: ' translate(-50%,-50%)',
          transform: 'translate(-50%,-50%)'
        }}>
        <h1
          className="text-center"
          style={{ fontSize: '15rem', color: '#525252', marginBottom: '-30px' }}>
          404
        </h1>
        <h2 className="text-center" style={{ fontSize: '50px', color: '#525252' }}>
          Not Found
        </h2>
        <p
          className="text-center"
          style={{ fontSize: '14px', color: '#525252', marginTop: '10px' }}>
          The requested resource couldn&apos;t be found on this server
        </p>
      </div>
    </div>
  </>
);

export default Error404Screen;
