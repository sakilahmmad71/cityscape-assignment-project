import DashboardComponent from '../components/dashboard/DashboardComponent';
import useLocalStorage from '../hooks/useLocalStorage';

const DashboardScreen = () => {
  const [userInformation] = useLocalStorage('supabase.auth.token');

  return (
    <div>
      <DashboardComponent profile={userInformation?.currentSession?.user?.user_metadata} />
    </div>
  );
};

export default DashboardScreen;
