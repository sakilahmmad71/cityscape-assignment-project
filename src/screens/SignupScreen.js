import { useState } from 'react';
import { Redirect } from 'react-router-dom';
import SignupComponent from '../components/authentication/SignupComponent';
import { SuccessNotification } from '../components/common/ToastNotification';
import { supabase } from '../config/supabaseClient';
import useLocalStorage from '../hooks/useLocalStorage';

const SignupScreen = () => {
  const [requestStatus, setRequestStatus] = useState({ loading: false, error: '', success: false });
  const [userInformation] = useLocalStorage('supabase.auth.token');

  const handleSignupUser = async (data, reset) => {
    const { email, password, firstname, lastname } = data;

    try {
      setRequestStatus((prevState) => ({ ...prevState, loading: true, error: '' }));
      const { user, error } = await supabase.auth.signUp(
        { email, password },
        { data: { firstname, lastname } }
      );

      if (user) {
        setRequestStatus((prevState) => ({ ...prevState, loading: false, success: true }));
        SuccessNotification('Signup successfully done, Now sign in!');
        if (typeof reset === 'function') {
          reset();
        }
      } else {
        console.log(error);
        setRequestStatus((prevState) => ({
          ...prevState,
          loading: false,
          error: error?.message
        }));
      }
    } catch (error) {
      setRequestStatus((prevState) => ({ ...prevState, loading: false, error: error?.message }));
    }
  };

  if (userInformation && userInformation?.currentSession?.access_token) {
    return <Redirect to="/" />;
  }

  return (
    <div>
      <SignupComponent
        handleSignupUser={handleSignupUser}
        loading={requestStatus?.loading}
        success={requestStatus?.success}
        error={requestStatus?.error}
      />
    </div>
  );
};

export default SignupScreen;
