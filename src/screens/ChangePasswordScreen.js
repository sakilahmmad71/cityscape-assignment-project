import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import ChangePasswordComponent from '../components/change-password/ChangePasswordComponent';
import { SuccessNotification } from '../components/common/ToastNotification';
import { supabase } from '../config/supabaseClient';
import useLocalStorage from '../hooks/useLocalStorage';

const ChangePasswordScreen = () => {
  const [requestStatus, setRequestStatus] = useState({ loading: false, error: '', success: false });
  const [userInformation] = useLocalStorage('supabase.auth.token');
  const history = useHistory();

  const changeUserPassword = async (data) => {
    const { password } = data;

    try {
      setRequestStatus((prevState) => ({ ...prevState, loading: true, error: '' }));
      const { user, error } = await supabase.auth.update({ password });

      if (user) {
        setRequestStatus((prevState) => ({ ...prevState, loading: false, success: true }));
        SuccessNotification('Password updated successfully!');

        setTimeout(() => {
          localStorage.removeItem('supabase.auth.token');
          localStorage.clear();
          history.push('/');
          window.location.reload();
        }, 1000);
      } else {
        console.log(error);
        setRequestStatus((prevState) => ({
          ...prevState,
          loading: false,
          error: error?.message
        }));
      }
    } catch (error) {
      setRequestStatus((prevState) => ({ ...prevState, loading: false, error: error?.message }));
    }
  };

  return (
    <div>
      <ChangePasswordComponent
        profile={userInformation?.currentSession?.user}
        loading={requestStatus?.loading}
        success={requestStatus?.success}
        error={requestStatus?.error}
        changeUserPassword={changeUserPassword}
      />
    </div>
  );
};

export default ChangePasswordScreen;
