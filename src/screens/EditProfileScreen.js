import { useState } from 'react';
import { SuccessNotification } from '../components/common/ToastNotification';
import EditProfileComponent from '../components/edit-profile/EditProfileComponent';
import { supabase } from '../config/supabaseClient';
import useLocalStorage from '../hooks/useLocalStorage';

const EditProfileScreen = () => {
  const [requestStatus, setRequestStatus] = useState({ loading: false, error: '', success: false });
  const [userInformation, setUserInformation] = useLocalStorage('supabase.auth.token');

  const editUserProfile = async (data) => {
    const { firstname, lastname, phone } = data;

    try {
      setRequestStatus((prevState) => ({ ...prevState, loading: true, error: '' }));
      const { user, error } = await supabase.auth.update({ data: { firstname, lastname }, phone });

      if (user) {
        setRequestStatus((prevState) => ({ ...prevState, loading: false, success: true }));
        SuccessNotification('Profile updated successfully!');

        // Setting up updated data
        setUserInformation((prevState) => ({
          ...prevState,
          currentSession: {
            ...prevState.currentSession,
            user: {
              ...prevState.currentSession.user,
              phone,
              user_metadata: user.user_metadata
            }
          }
        }));
      } else {
        console.log(error);
        setRequestStatus((prevState) => ({
          ...prevState,
          loading: false,
          error: error?.message
        }));
      }
    } catch (error) {
      setRequestStatus((prevState) => ({ ...prevState, loading: false, error: error?.message }));
    }
  };

  return (
    <div>
      <EditProfileComponent
        profile={userInformation?.currentSession?.user}
        loading={requestStatus?.loading}
        success={requestStatus?.success}
        error={requestStatus?.error}
        editUserProfile={editUserProfile}
      />
    </div>
  );
};

export default EditProfileScreen;
