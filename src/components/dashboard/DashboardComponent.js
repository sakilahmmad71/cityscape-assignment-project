import 'bootstrap/dist/js/bootstrap.esm';
import PropTypes from 'prop-types';
import DummyText from '../common/DummyText';
import SubTitle from '../common/SubTitle';
import Title from '../common/Title';

const DashboardComponent = ({ profile = {} }) => {
  return (
    <>
      <Title title={`Welcome, ${profile?.firstname} ${profile?.lastname}`} />

      <div className="row">
        <div className="col-md-10 col-sm-12 mx-auto">
          <SubTitle title="Read about our terms and conditions" />
          <DummyText />
          <DummyText />
          <DummyText />
          <DummyText />
        </div>
      </div>
    </>
  );
};

DashboardComponent.propTypes = {
  profile: PropTypes.object.isRequired
};

export default DashboardComponent;
