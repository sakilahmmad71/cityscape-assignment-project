import { yupResolver } from '@hookform/resolvers/yup';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import Button from '../common/Button';
import DummyText from '../common/DummyText';
import Error from '../common/Error';
import Input from '../common/Input';
import Title from '../common/Title';

const ChangePasswordSchema = yup.object().shape({
  password: yup.string().required('Password is required field.').min(3).max(30),
  email: yup.string().email().required('Email or Username is required field.')
});

const ChangePasswordComponent = ({
  profile = {},
  loading = false,
  error = '',
  changeUserPassword = null,
  // eslint-disable-next-line no-unused-vars
  success = false
}) => {
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(ChangePasswordSchema)
  });

  const handleChangePassword = (data) => {
    if (typeof changeUserPassword === 'function') {
      changeUserPassword(data);
    }
  };

  return (
    <>
      <Title
        title={`Change Password (${profile?.user_metadata?.firstname} ${profile?.user_metadata?.lastname})`}
      />

      <div className="row">
        <div className="col-md-8 col-sm-12 mx-auto d-flex flex-column justify-content-center align-items-center">
          {error && <Error error={error} />}

          <div className="profile-image">
            <div className="height-150 width-150 rounded-circle bg-off-white">
              <i className="fa fa-user m-2 p-4 fs-100 color-dark-white"></i>
            </div>
          </div>

          <form className="forms container" onSubmit={handleSubmit(handleChangePassword)}>
            <fieldset disabled={loading}>
              <div className="row">
                <div className="col-md-8 mx-auto">
                  <Input
                    classNames="my-3"
                    testId="Email"
                    type="email"
                    name="email"
                    label="Email"
                    placeholder="example@email.com"
                    defaultInputValue={profile?.email}
                    inputRef={register}
                    error={errors.email}
                    readOnly
                  />

                  <Input
                    testId="password"
                    type="password"
                    name="password"
                    label="Password"
                    placeholder="Type password"
                    inputRef={register}
                    error={errors.password}
                    isRequired
                  />
                </div>
              </div>

              <div className="row my-4">
                <div className="col-md-8 mx-auto col-sm-12 col-xs-12 text-light">
                  <div className="mx-auto">
                    <Button
                      isPillButton={false}
                      testId="login"
                      type="submit"
                      classNames="btn rounded-3 text-white w-100 bg-light-blue"
                      isDisablable={loading}
                      label={`${loading ? 'Changing Password...' : 'Change Password'}`}
                    />
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>

      <div className="row my-3">
        <div className="col-md-10 col-sm-12 mx-auto">
          <DummyText />
          <DummyText />
        </div>
      </div>
    </>
  );
};

ChangePasswordComponent.propTypes = {
  profile: PropTypes.object.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
  success: PropTypes.bool,
  changeUserPassword: PropTypes.func.isRequired
};

export default ChangePasswordComponent;
