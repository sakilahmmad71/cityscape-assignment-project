import { yupResolver } from '@hookform/resolvers/yup';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import Button from '../common/Button';
import DummyText from '../common/DummyText';
import Error from '../common/Error';
import Input from '../common/Input';
import Title from '../common/Title';

const EditProfileSchema = yup.object().shape({
  firstname: yup.string().required('First Name is required field.').min(3).max(30),
  lastname: yup.string().required('Last Name is required field.').min(3).max(30),
  email: yup.string().email().required('Email or Username is required field.'),
  phone: yup.string()
});

const EditProfileComponent = ({
  profile = {},
  loading = false,
  error = '',
  editUserProfile = null,
  // eslint-disable-next-line no-unused-vars
  success = false
}) => {
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(EditProfileSchema)
  });

  const handleFileButton = () => {
    document.getElementById('getFile').click();
  };

  const handleEditProfile = (data) => {
    if (typeof editUserProfile === 'function') {
      editUserProfile(data);
    }
  };

  return (
    <>
      <Title
        title={`Edit Profile (${profile?.user_metadata?.firstname} ${profile?.user_metadata?.lastname})`}
      />

      <div className="row">
        <div className="col-md-8 col-sm-12 mx-auto d-flex flex-column justify-content-center align-items-center">
          {error && <Error error={error} />}

          <div className="profile-image">
            <div className="height-150 width-150 rounded-circle bg-off-white">
              <i className="fa fa-user m-2 p-4 fs-100 color-dark-white"></i>
            </div>
          </div>

          <div className="container my-2">
            <div className="row">
              <div className="col-md-8 mx-auto">
                <button
                  className="form-control-plaintext border border-light-dark fw-normal p-2 rounded-3 text-center"
                  onClick={handleFileButton}>
                  Edit picture
                </button>
                <input type="file" id="getFile" style={{ display: 'none' }} />
              </div>
            </div>
          </div>

          <form className="forms container" onSubmit={handleSubmit(handleEditProfile)}>
            <fieldset disabled={loading}>
              <div className="row">
                <div className="col-md-8 mx-auto">
                  <Input
                    inputClassNames="form-control-plaintext fw-normal p-2 rounded-3"
                    classNames="my-3"
                    testId="firstname"
                    type="text"
                    name="firstname"
                    label="Firstname"
                    placeholder="John"
                    defaultInputValue={profile?.user_metadata?.firstname}
                    inputRef={register}
                  />

                  <Input
                    testId="lastname"
                    type="text"
                    name="lastname"
                    label="Lastname"
                    placeholder="Doe"
                    defaultInputValue={profile?.user_metadata?.lastname}
                    inputRef={register}
                  />

                  <Input
                    classNames="my-3"
                    testId="Email"
                    type="email"
                    name="email"
                    label="Email"
                    placeholder="example@email.com"
                    defaultInputValue={profile?.email}
                    inputRef={register}
                    error={errors.email}
                    readOnly
                  />

                  <Input
                    testId="phone"
                    type="text"
                    name="phone"
                    label="Phone"
                    placeholder="8801XXXXXXXXXXX"
                    defaultInputValue={profile?.phone}
                    error={errors.phone}
                    inputRef={register}
                  />
                </div>
              </div>

              <div className="row my-4">
                <div className="col-md-8 mx-auto col-sm-12 col-xs-12 text-light">
                  <div className="mx-auto">
                    <Button
                      isPillButton={false}
                      testId="login"
                      type="submit"
                      classNames="btn rounded-3 text-white w-100 bg-light-blue"
                      isDisablable={loading}
                      label={`${loading ? 'Editing Profile...' : 'Edit profile'}`}
                    />
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>

      <div className="row my-3">
        <div className="col-md-10 col-sm-12 mx-auto">
          <DummyText />
          <DummyText />
        </div>
      </div>
    </>
  );
};

EditProfileComponent.propTypes = {
  profile: PropTypes.object.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
  success: PropTypes.bool,
  editUserProfile: PropTypes.func.isRequired
};

export default EditProfileComponent;
