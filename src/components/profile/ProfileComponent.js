import PropTypes from 'prop-types';
import DummyText from '../common/DummyText';
import Input from '../common/Input';
import Title from '../common/Title';

const ProfileComponent = ({ profile = {} }) => {
  return (
    <>
      <Title
        title={`Profile (${profile?.user_metadata?.firstname} ${profile?.user_metadata?.lastname})`}
      />

      <div className="row">
        <div className="col-md-8 col-sm-12 mx-auto d-flex flex-column justify-content-center align-items-center">
          <div className="profile-image">
            <div className="height-150 width-150 rounded-circle bg-off-white">
              <i className="fa fa-user m-2 p-4 fs-100 color-dark-white"></i>
            </div>
          </div>

          <div className="forms container my-3">
            <div className="row">
              <div className="col-md-8 mx-auto">
                <Input
                  classNames="my-3"
                  testId="firstname"
                  type="text"
                  name="firstname"
                  label="Firstname"
                  placeholder="John"
                  defaultInputValue={profile?.user_metadata?.firstname}
                  readOnly
                />

                <Input
                  testId="lastname"
                  type="text"
                  name="lastname"
                  label="Lastname"
                  placeholder="Doe"
                  defaultInputValue={profile?.user_metadata?.lastname}
                  readOnly
                />

                <Input
                  classNames="my-3"
                  testId="Email"
                  type="email"
                  name="email"
                  label="Email"
                  placeholder="example@email.com"
                  defaultInputValue={profile?.email}
                  readOnly
                />

                <Input
                  testId="phone"
                  type="text"
                  name="phone"
                  label="Phone"
                  defaultInputValue={profile?.phone || 'N/A'}
                  readOnly
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row my-3">
        <div className="col-md-10 col-sm-12 mx-auto">
          <DummyText />
          <DummyText />
        </div>
      </div>
    </>
  );
};

ProfileComponent.propTypes = {
  profile: PropTypes.object.isRequired
};

export default ProfileComponent;
