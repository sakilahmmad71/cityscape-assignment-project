/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { yupResolver } from '@hookform/resolvers/yup';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import * as yup from 'yup';
import Button from '../common/Button';
import Error from '../common/Error';
import Input from '../common/Input';

const AuthenticationSchema = yup.object().shape({
  email: yup.string().email().required('Email or Username is required field.'),
  password: yup.string().required('Password is required field.').min(6).max(30)
});

const LoginComponent = ({
  handleLoginUser = null,
  loading = false,
  error = '',
  success = false
}) => {
  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(AuthenticationSchema)
  });

  const handleLogin = (data) => {
    if (typeof handleLoginUser === 'function') {
      handleLoginUser(data, reset);
    }
  };

  // const handleGoogleLogin = () => {
  //   if (typeof handleLoginUser === 'function') {
  //     handleLoginUser('GOOGLE', {});
  //   }
  // };

  return (
    <div className="container-fluid login-page">
      <div className="login-page-overlay"></div>
      <div className="row">
        <div className="login bg-white rounded-3">
          <div className="text-center text-dark">
            <h4>SIGN IN</h4>
            <p>Sign in to your account and access valuable resources </p>
          </div>

          <hr className="dropdown-divider color-light-blue border border-2" />

          {error && <Error error={error} />}

          <form onSubmit={handleSubmit(handleLogin)} className="py-2">
            <fieldset disabled={loading}>
              <Input
                classNames="py-3"
                testId="email"
                type="email"
                name="email"
                inputRef={register}
                error={errors.email}
                label="Email"
                placeholder="example@email.com"
                isRequired
              />

              <Input
                testId="password"
                type="password"
                name="password"
                inputRef={register}
                error={errors.password}
                label="Password"
                placeholder="Type Password"
                isRequired
              />

              <div className="form-group row text-dark py-2">
                <label
                  className="control-label col-md-3 col-sm-3 col-xs-12 d-flex color-mid-dark"
                  htmlFor="username"></label>
                <div className="col-md-9 col-sm-9 col-xs-12">
                  <div className="input-group d-flex justify-content-end">
                    <a href="" className="text-dark text-decoration-none text-end">
                      Forget <span className="color-light-blue">username/password?</span>
                    </a>
                  </div>
                </div>
              </div>

              <div className="row pt-1">
                <div className="col-md-12 col-sm-12 col-xs-12 text-light">
                  <div className="mx-auto">
                    <Button
                      testId="login"
                      classNames="btn rounded-3 text-white w-100 bg-light-blue"
                      isDisablable={loading}
                      label={`${loading ? 'Logging In...' : 'Log In'}`}
                    />
                  </div>
                </div>
              </div>
            </fieldset>
          </form>

          <h6 className="text-center mt-1">OR</h6>

          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12 text-light">
              <div className="mx-auto">
                <Button
                  // handleOnClick={handleGoogleLogin}
                  testId="login-with-google"
                  classNames="btn rounded-3 color-mid-dark w-100 bg-off-white"
                  isDisablable={loading}
                  label={
                    <>
                      <div className="d-flex justify-content-center align-items center">
                        <img
                          src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/24px-Google_%22G%22_Logo.svg.png"
                          alt="Google logo"
                          className="mx-2"
                        />
                        <span>Sign in with google</span>
                      </div>
                    </>
                  }
                />
              </div>
            </div>
          </div>

          <div className="row pt-5">
            <div className="col-md-12 col-sm-12 col-xs-12 text-dark d-flex flex-column">
              <span className="fs-6 text-center">Dont have an account?</span>
              <span className="fs-5 text-center">
                <Link className="text-decoration-none" to="/signup" data-test="to-signup-page">
                  SIGN UP NOW
                </Link>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

LoginComponent.propTypes = {
  handleLoginUser: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
  success: PropTypes.bool
};

export default LoginComponent;
