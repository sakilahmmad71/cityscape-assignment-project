/* eslint-disable no-unused-vars */
import { yupResolver } from '@hookform/resolvers/yup';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import * as yup from 'yup';
import Button from '../common/Button';
import Error from '../common/Error';
import Input from '../common/Input';

const AuthenticationSchema = yup.object().shape({
  firstname: yup.string().required('First Name is required field.').min(3).max(30),
  lastname: yup.string().required('Last Name is required field.').min(3).max(30),
  email: yup.string().email().required('Email or Username is required field.'),
  password: yup.string().required('Password is required field.').min(6).max(30)
});

// eslint-disable-next-line prettier/prettier
const SignupComponent = ({
  handleSignupUser = null,
  loading = false,
  error = '',
  success = false
}) => {
  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(AuthenticationSchema)
  });

  const handleSignup = (data) => {
    if (typeof handleSignupUser === 'function') {
      handleSignupUser(data, reset);
    }
  };

  return (
    <div className="container-fluid login-page">
      <div className="login-page-overlay"></div>
      <div className="row">
        <div className="login bg-white rounded-3">
          <div className="text-center text-dark">
            <h4>SIGN UP</h4>
            <p>Sign up new account and access valuable resources </p>
          </div>

          <hr className="dropdown-divider color-light-blue border border-2" />

          {error && <Error error={error} />}

          <form onSubmit={handleSubmit(handleSignup)} className="py-2">
            <fieldset disabled={loading}>
              <Input
                classNames="py-3"
                testId="firstname"
                type="text"
                name="firstname"
                inputRef={register}
                error={errors.firstname}
                label="Firstname"
                placeholder="John"
                isRequired
              />

              <Input
                testId="lastname"
                type="text"
                name="lastname"
                inputRef={register}
                error={errors.lastname}
                label="Lastname"
                placeholder="Doe"
                isRequired
              />

              <Input
                classNames="py-3"
                testId="email"
                type="email"
                name="email"
                inputRef={register}
                error={errors.email}
                label="Email"
                placeholder="example@email.com"
                isRequired
              />

              <Input
                testId="password"
                type="password"
                name="password"
                inputRef={register}
                error={errors.password}
                label="Password"
                placeholder="Type Password"
                isRequired
              />

              <div className="row pt-4">
                <div className="col-md-12 col-sm-12 col-xs-12 text-light">
                  <div className="mx-auto">
                    <Button
                      testId="signup"
                      classNames="btn rounded-3 text-white w-100 bg-light-blue"
                      isDisablable={loading}
                      label={`${loading ? 'Signing Up...' : 'Sign Up'}`}
                    />
                  </div>
                </div>
              </div>
            </fieldset>
          </form>

          <h6 className="text-center mt-1">OR</h6>

          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12 text-light">
              <div className="mx-auto">
                <Button
                  testId="signup-with-google"
                  classNames="btn rounded-3 color-mid-dark w-100 bg-off-white"
                  isDisablable={loading}
                  label={
                    <>
                      <div className="d-flex justify-content-center align-items center">
                        <img
                          src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/24px-Google_%22G%22_Logo.svg.png"
                          alt="Google logo"
                          className="mx-2"
                        />
                        <span>Sign up with google</span>
                      </div>
                    </>
                  }
                />
              </div>
            </div>
          </div>

          <div className="row pt-5">
            <div className="col-md-12 col-sm-12 col-xs-12 text-dark d-flex flex-column">
              <span className="fs-6 text-center">Already have an account?</span>
              <span className="fs-5 text-center">
                <Link className="text-decoration-none" to="/" data-test="to-login-page">
                  SIGN IN NOW
                </Link>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

SignupComponent.propTypes = {
  handleSignupUser: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
  success: PropTypes.bool
};

export default SignupComponent;
