const DummyText = () => {
  return (
    <p className="fw-light text-justify color-light-dark">
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut, laudantium vero harum amet ipsa
      enim optio possimus asperiores animi adipisci impedit molestias veritatis officiis ab
      excepturi quia sapiente error repellat vitae ipsam! Incidunt ea inventore doloremque. Rem,
      iusto facere? Asperiores eligendi incidunt exercitationem cumque quae commodi optio animi vero
      expedita!. Rem, iusto facere? Asperiores eligendi incidunt exercitationem cumque quae commodi
      optio animi vero expedita!
    </p>
  );
};

export default DummyText;
