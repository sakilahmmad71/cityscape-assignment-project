const Footer = () => (
  <div>
    <div className="container-fluid">
      <div className="footer text-center py-2 mx-1 footer">
        <p>Designed and developed by Shakil Ahmed&copy;2021</p>
      </div>
    </div>
  </div>
);

export default Footer;
