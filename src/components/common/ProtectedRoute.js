import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';
import useLocalStorage from '../../hooks/useLocalStorage';
import ApplicationWrapper from './ApplicationWrapper';

const ProtectedRoute = ({ component: Component, ...rest }) => {
  const [userInformation] = useLocalStorage('supabase.auth.token');

  return (
    <>
      <Route
        {...rest}
        render={(props) =>
          userInformation && userInformation?.currentSession?.access_token ? (
            <ApplicationWrapper>
              <Component {...props} />
            </ApplicationWrapper>
          ) : (
            <Redirect to="/" />
          )
        }
      />
    </>
  );
};

ProtectedRoute.propTypes = {
  component: PropTypes.elementType,
  rest: PropTypes.object
};

export default ProtectedRoute;
