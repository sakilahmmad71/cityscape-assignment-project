import { useEffect, useState } from 'react';
import OfflineAlert from './OfflineAlert';

const Offline = () => {
  const [isOnline, toggleOnlineStatus] = useState(true);

  useEffect(() => {
    const handleOnlineOrOfflineStatus = (event) => {
      if (event.type === 'online') {
        toggleOnlineStatus(true);
      } else if (event.type === 'offline') {
        toggleOnlineStatus(false);
      }
    };

    window.addEventListener('online', handleOnlineOrOfflineStatus);
    window.addEventListener('offline', handleOnlineOrOfflineStatus);

    return () => {
      window.removeEventListener('online', handleOnlineOrOfflineStatus);
      window.removeEventListener('offline', handleOnlineOrOfflineStatus);
    };
  }, []);

  if (isOnline) {
    return <></>;
  }

  return (
    <>
      <div>
        <h5
          style={{
            fontWeight: 'bolder',
            background: '#ffc107',
            textAlign: 'center',
            color: '#ffffff',
            margin: 0,
            padding: 5
          }}>
          <i className="fa fa-exclamation-circle" /> No internet connection available.
        </h5>
      </div>

      <OfflineAlert isOnline={isOnline} />
    </>
  );
};

export default Offline;
