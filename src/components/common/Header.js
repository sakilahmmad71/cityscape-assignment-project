import 'bootstrap/dist/js/bootstrap.esm';
import { Link, NavLink } from 'react-router-dom';
import { supabase } from '../../config/supabaseClient';
import useLocalStorage from '../../hooks/useLocalStorage';
import { SuccessNotification } from './ToastNotification';

const Header = () => {
  const [userInformation] = useLocalStorage('supabase.auth.token');

  const handleLogOut = async () => {
    const { error } = await supabase.auth.signOut();

    if (!error) {
      SuccessNotification('Log out successfully done!');
      localStorage.removeItem('supabase.auth.token');
      localStorage.clear();
      window.location.reload();
    }
  };

  return (
    <div className="row">
      <nav className="navbar navbar-light bg-white px-4 height-75">
        <div className="container-fluid">
          <div className="nav-brand">
            <Link className="navbar-brand font-monospace" to="/" data-test="brand-name">
              <span className="color-light-blue">C</span>ITYSCAPE
            </Link>
          </div>

          <div className="nav-menu">
            <div className="dropdown">
              <a
                className="user-profile d-flex justify-content-center align-items-center color-mid-dark dropdown-toggle text-decoration-none"
                href="#"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false">
                <div className="height-50 width-50 rounded-circle bg-off-white">
                  <i className="fa fa-user p-3 fs-4 color-dark-white"></i>
                </div>
                <p className="text-center pt-3 mx-2">
                  {' '}
                  {userInformation?.currentSession?.user?.user_metadata?.firstname}{' '}
                  {userInformation?.currentSession?.user?.user_metadata?.lastname}
                </p>
              </a>

              <ul
                className="dropdown-menu dropdown-menu-light"
                aria-labelledby="navbarDarkDropdownMenuLink">
                <li>
                  <NavLink className="dropdown-item" to="/profile" exact data-test="show-profile">
                    Show Profile
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className="dropdown-item"
                    to="/edit-profile"
                    exact
                    data-test="edit-profile">
                    Edit Profile
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    className="dropdown-item"
                    to="/change-password"
                    exact
                    data-test="change-password">
                    Change Password
                  </NavLink>
                </li>
                <li onClick={handleLogOut}>
                  <Link className="dropdown-item" to="/" exact data-test="logout">
                    Log Out
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Header;
