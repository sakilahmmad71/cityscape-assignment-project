import PropTypes from 'prop-types';
import Footer from '../common/Footer';
import Header from '../common/Header';

const ApplicationWrapper = ({ children = null }) => (
  <>
    <div className="container-fluid">
      <Header />
      <div className="container bg-white mx-auto min-height-90vh rounded-3 m-3">{children}</div>
      <Footer />
    </div>
  </>
);

ApplicationWrapper.propTypes = {
  children: PropTypes.object.isRequired
};

export default ApplicationWrapper;
