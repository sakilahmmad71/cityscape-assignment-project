import PropTypes from 'prop-types';

const SubTitle = ({ title = '' }) => {
  return <p className="text-center color-light-dark fs-5">{title}</p>;
};

SubTitle.propTypes = {
  title: PropTypes.string.isRequired
};

export default SubTitle;
