import { useEffect } from 'react';
import swal from 'sweetalert';

const FallBackError = () => {
  useEffect(() => {
    swal({
      title: 'Error detected while running application.',
      text: 'Clear cache and reload by pressing CTRL+SHIFT+R',
      icon: 'error',
      buttons: ['Cancel', 'Try again'],
      dangerMode: true
    }).then((clicked) => {
      if (clicked) {
        window.location.assign('/');
      }
    });
  }, []);

  return <></>;
};

export default FallBackError;
