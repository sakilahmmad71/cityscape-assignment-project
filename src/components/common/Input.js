import PropTypes from 'prop-types';
import React from 'react';
import InvalidInput from './InvalidInput';

const Input = ({
  classNames = '',
  iconClassNames = '',
  inputClassNames = '',
  label = '',
  defaultInputValue = null,
  isRequired = false,
  type = 'text',
  name = '',
  inputRef = null,
  error = null,
  placeholder = '',
  handleOnChange = null,
  handleOnBlur = null,
  hasIcon = false,
  initialIconName = null,
  handleClickOnIcon = null,
  min = 0,
  readOnly = false,
  acceptFiles = null,
  inputMode = '',
  step = null,
  testId = null
}) => (
  <div className={`form-group row ${classNames}`}>
    <label
      className="control-label col-md-3 col-sm-3 col-xs-12 d-flex mt-2 color-mid-dark"
      htmlFor={name}>
      {label} <span className="color-mid-dark">{isRequired && '*'}</span>
    </label>

    <div className="col-md-9 col-sm-9 col-xs-12">
      <div className="input-group">
        {hasIcon && (
          <span onClick={handleClickOnIcon} className="input-group-text" role="button">
            <i className={`${initialIconName} ${iconClassNames}`} />
          </span>
        )}

        <input
          id={name}
          data-test={testId}
          type={type}
          defaultValue={defaultInputValue}
          className={`p-2 form-control bg-off-white border border-0 rounded-pill ${inputClassNames}`}
          name={name}
          placeholder={placeholder}
          ref={inputRef}
          onChange={handleOnChange}
          onBlur={handleOnBlur}
          min={min}
          readOnly={readOnly}
          accept={acceptFiles}
          inputMode={inputMode}
          step={step}
        />
      </div>

      {error && <InvalidInput error={error} />}
    </div>
  </div>
);

Input.propTypes = {
  classNames: PropTypes.string,
  iconClassNames: PropTypes.string,
  inputClassNames: PropTypes.string,
  label: PropTypes.string,
  defaultInputValue: PropTypes.string,
  isRequired: PropTypes.bool,
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  inputRef: PropTypes.func,
  error: PropTypes.object,
  placeholder: PropTypes.string,
  handleOnChange: PropTypes.bool,
  handleOnBlur: PropTypes.bool,
  hasIcon: PropTypes.bool,
  initialIconName: PropTypes.bool,
  handleClickOnIcon: PropTypes.bool,
  min: PropTypes.number,
  readOnly: PropTypes.bool,
  acceptFiles: PropTypes.bool,
  inputMode: PropTypes.string,
  step: PropTypes.bool,
  testId: PropTypes.string
};

export default Input;
