import PropTypes from 'prop-types';

const Title = ({ title = '' }) => {
  return (
    <div className="row" data-test="page-title">
      <div className="col-md-12 col-sm-12">
        <div className="login-welcome pt-5 pb-4">
          <h2 className="text-center color-light-dark">{title}</h2>
          <hr className="dropdown-divider color-light-blue border border-2" />
        </div>
      </div>
    </div>
  );
};

Title.propTypes = {
  title: PropTypes.string.isRequired
};

export default Title;
