import toast from 'react-hot-toast';

const options = {
  duration: 4000,
  style: {
    borderRadius: '10px',
    background: '#333',
    color: '#fff'
  }
};

export const SuccessNotification = (message = 'Successfully Done!') => {
  toast.success(message, options);
};

export const ErrorNotification = (message = 'This Did Not Worked!') => {
  toast.error(message, options);
};
