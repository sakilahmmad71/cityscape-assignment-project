import PropTypes from 'prop-types';

const Button = ({
  isPillButton = true,
  label = '',
  isDisablable = false,
  classNames = '',
  testId = null,
  type = 'submit',
  handleOnClick = null
}) => (
  <button
    onClick={handleOnClick}
    data-test={testId}
    type={type}
    className={`btn p-2 shadow-sm ${isPillButton ? 'rounded-pill' : 'rounded-3'} ${classNames}`}
    disabled={isDisablable}>
    {label}{' '}
  </button>
);

Button.propTypes = {
  isPillButton: PropTypes.bool,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.elementType])
    .isRequired,
  isDisablable: PropTypes.bool,
  classNames: PropTypes.string,
  type: PropTypes.string,
  testId: PropTypes.string,
  handleOnClick: PropTypes.func
};

export default Button;
