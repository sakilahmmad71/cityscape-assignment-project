import PropTypes from 'prop-types';
import { VscError } from 'react-icons/vsc';

const InvalidInput = ({ error = '' }) => (
  <div className="text-danger d-flex align-items-center mt-1" role="alert">
    <VscError color="text-danger" className="mx-1" fontSize="1.2em" />{' '}
    <div>{error.message ? error.message : error}</div>
  </div>
);

InvalidInput.propTypes = {
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired
};

export default InvalidInput;
