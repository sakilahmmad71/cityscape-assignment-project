import { Route, Switch } from 'react-router-dom';
import ProtectedRoute from '../components/common/ProtectedRoute';
import useLocalStorage from '../hooks/useLocalStorage';
import ChangePasswordScreen from '../screens/ChangePasswordScreen';
import DashboardScreen from '../screens/DashboardScreen';
import EditProfileScreen from '../screens/EditProfileScreen';
import Error404Screen from '../screens/Error404Screen';
import LoginScreen from '../screens/LoginScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SignupScreen from '../screens/SignupScreen';

const DefaultApplication = () => {
  const [userInformation] = useLocalStorage('supabase.auth.token');

  if (!userInformation && !userInformation?.currentSession?.access_token) {
    return (
      <>
        <Switch>
          <Route path="/" exact component={LoginScreen} />
          <Route path="/signup" exact component={SignupScreen} />
          <Route path="*" exact component={Error404Screen} />
        </Switch>
      </>
    );
  }

  return (
    <>
      <Switch>
        <ProtectedRoute path="/" exact component={DashboardScreen} />
        <ProtectedRoute path="/profile" exact component={ProfileScreen} />
        <ProtectedRoute path="/edit-profile" exact component={EditProfileScreen} />
        <ProtectedRoute path="/change-password" exact component={ChangePasswordScreen} />
        <Route path="/signup" exact component={SignupScreen} />
        <Route path="*" exact component={Error404Screen} />
      </Switch>
    </>
  );
};

export default DefaultApplication;
