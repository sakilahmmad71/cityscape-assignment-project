import { ErrorBoundary } from 'react-error-boundary';
import { Toaster } from 'react-hot-toast';
import { BrowserRouter as Router } from 'react-router-dom';
import FallBackError from './components/common/FallbackError';
import Offline from './components/common/Offline';
import DefaultApplication from './containers/DefaultApplication';

const toasterStyles = {
  style: {
    borderRadius: '10px',
    background: '#333',
    color: '#fff'
  }
};

const App = () => {
  const handleBoundayError = (error, errorInfo) => {
    console.log('Error Occured ', error, errorInfo);
  };

  return (
    <>
      <Offline />
      <Router>
        <ErrorBoundary FallbackComponent={FallBackError} onError={handleBoundayError}>
          <DefaultApplication />
          <Toaster position="bottom-center" reverseOrder toastOptions={toasterStyles} />
        </ErrorBoundary>
      </Router>
    </>
  );
};

export default App;
