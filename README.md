## Cityscape Assignment Project

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

#### This is assignment project for demo purpose.
This project has created by followed 10 steps and best practices as I know by study several articles:

- Documentation Structure -  First documented how this project will be structure by simply writing on paper
- Wireframing Design - After thinking the structure of the application and pages then drawn page structure on paper
- UX Design - In this step created UX design according to the wireframe structure and modified as needed
- Prototype of UI - Created static pages by writing HTML and CSS from UX designs
- Responsiveness- In this step created the UI pages responsive because 67% of users are mobile and other device user
- React Components - Make components and pages from UI's
- Feature Implementation - All the components ware dump component then implemented features on pages as needed
- API Integration - After feature implementation UI needs to represent the data which provided by API's
- Testing - For ensuring everything is working or not, we'd to test by hand and it was really annoying at this step implemented testing like Unit, Integration and E2E tests
- Code Refactoring - After testing sometimes source code looks really messy then code refactoring comes into play which makes code redable and apply Design Patterns
- Production Deployment - After all these tests and User Acceptance Tests application deploy to production

## Features

- Authentication (Sign in & Sign up)
- Edit profile infromations
- Change password

## Tech
Technologies used for this project

- JavaScript - Programming Language
- React.js - UI Library
- Figma - UX design
- HTML, CSS, Bootstrap - UI boilerplate
- supabase - Open source Backend as a service
- Github - Source code management

## Installation

Open your terminal and simply clone this repo.

```sh
git clone https://gitlab.com/sakilahmmad71/cityscape-assignment-project.git -b development
```

Then go to the project directory.

```sh
cd cityscape-assignment-project
```

requires [Node.js](https://nodejs.org/) v12+ to run.

Install the dependencies and devDependencies.

```sh
npm install
```

For run the application.
```sh
npm start
```

Verify the server running by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:3000
or
http://localhost:3000
```