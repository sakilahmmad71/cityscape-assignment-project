/* eslint-disable no-undef */

describe('Automated Testing of Login Page', () => {
  it('displays everythins inputs appears or not and try login with wrong credentials', () => {
    cy.visit('http://localhost:3000/');

    cy.contains('SIGN IN').should('exist');
    cy.contains('Forget username/password?').should('exist');
    cy.get('[data-test=email]').should('exist');
    cy.get('[data-test=password]').should('exist');
    cy.get('[data-test=login]').should('exist');
    cy.contains('OR').should('exist');
    cy.get('[data-test=login-with-google]').should('exist');
    cy.contains('Dont have an account?').should('exist');
    cy.contains('SIGN UP NOW').should('exist');

    cy.get('[data-test=email]').type('helloworld@gmail.com');
    cy.get('[data-test=password]').type('hello123');
    cy.get('[data-test=login]').click();

    cy.contains('Invalid login credentials').should('exist');
  });

  it('Test login to signup route change', () => {
    cy.get('[data-test=to-signup-page]').click();

    cy.url().should('include', '/signup');
    cy.visit('http://localhost:3000/');
  });

  it('Show invalid errors under inputs', () => {
    cy.get('[data-test=email]').clear();
    cy.get('[data-test=email]').type('hello@world');

    cy.get('[data-test=password]').clear();
    cy.get('[data-test=password]').type('hel');

    cy.get('[data-test=login]').click();

    cy.contains('email must be a valid email').should('exist');
    cy.contains('password must be at least 6 characters').should('exist');
  });

  it('Show invalid errors when email is empty and password more than 30 chars', () => {
    cy.get('[data-test=email]').clear();
    cy.get('[data-test=email]').type(' ');

    cy.get('[data-test=password]').clear();
    cy.get('[data-test=password]').type('helloworldisgoingtoexplodedsoonandlatersodontworry');

    cy.get('[data-test=login]').click();

    cy.contains('Email or Username is required field.').should('exist');
    cy.contains('password must be at most 30 characters').should('exist');
  });

  it('Place correct email and password to ensure tests working and login to dashboard', () => {
    cy.get('[data-test=email]').clear();
    cy.get('[data-test=email]').type('sakilahmmad71@gmail.com');

    cy.get('[data-test=password]').clear();
    cy.get('[data-test=password]').type('sakil123');

    cy.get('[data-test=login]').click();

    cy.get('[data-test=brand-name]').contains('CITYSCAPE').should('exist');
  });
});
