/* eslint-disable no-undef */

describe('Automated Testing of Signup Page', () => {
  it('displays everythins inputs appears or not and try signup with inappropriate informations', () => {
    cy.visit('http://localhost:3000/signup');

    cy.contains('SIGN UP').should('exist');
    cy.get('[data-test=firstname]').should('exist');
    cy.get('[data-test=lastname]').should('exist');
    cy.get('[data-test=email]').should('exist');
    cy.get('[data-test=password]').should('exist');
    cy.get('[data-test=signup]').should('exist');
    cy.contains('OR').should('exist');
    cy.get('[data-test=signup-with-google]').should('exist');
    cy.contains('Already have an account?').should('exist');
    cy.contains('SIGN IN NOW').should('exist');

    cy.get('[data-test=firstname]').type('hello');
    cy.get('[data-test=lastname]').type('world');
    cy.get('[data-test=email]').type('helloworld@123');
    cy.get('[data-test=password]').type('hel');
    cy.get('[data-test=signup]').click();

    cy.contains('email must be a valid email').should('exist');
    cy.contains('password must be at least 6 characters').should('exist');
  });

  it('Test signup to login route change', () => {
    cy.get('[data-test=to-login-page]').click();

    cy.url().should('eq', 'http://localhost:3000/');
    cy.visit('http://localhost:3000/signup');
  });

  it('Show invalid errors under inputs', () => {
    cy.get('[data-test=firstname]').clear();
    cy.get('[data-test=firstname]').type(' ');

    cy.get('[data-test=lastname]').clear();
    cy.get('[data-test=lastname]').type(' ');

    cy.get('[data-test=email]').clear();
    cy.get('[data-test=email]').type('123@123');

    cy.get('[data-test=password]').clear();
    cy.get('[data-test=password]').type(' ');

    cy.get('[data-test=signup]').click();

    cy.contains('firstname must be at least 3 characters').should('exist');
    cy.contains('lastname must be at least 3 characters').should('exist');
    cy.contains('email must be a valid email').should('exist');
    cy.contains('password must be at least 6 characters').should('exist');
  });

  it('Signup with appropriate informations', () => {
    cy.get('[data-test=firstname]').clear();
    cy.get('[data-test=firstname]').type('test');

    cy.get('[data-test=lastname]').clear();
    cy.get('[data-test=lastname]').type('user');

    cy.get('[data-test=email]').clear();
    cy.get('[data-test=email]').type('testuser@gmail.com');

    cy.get('[data-test=password]').clear();
    cy.get('[data-test=password]').type('test123');

    cy.get('[data-test=signup]').click();

    cy.contains('Signup successfully done, Now sign in!').should('exist');
  });
});
