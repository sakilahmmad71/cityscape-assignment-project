/* eslint-disable no-undef */

describe('Automated Testing of Dashboard Page', () => {
  it('displays everythins inputs appears or not and try login with wrong credentials', () => {
    cy.visit('http://localhost:3000/');

    cy.get('[data-test=email]').clear();
    cy.get('[data-test=email]').type('sakilahmmad71@gmail.com');

    cy.get('[data-test=password]').clear();
    cy.get('[data-test=password]').type('sakil123');

    cy.get('[data-test=login]').click();

    cy.get('[data-test=brand-name]').contains('CITYSCAPE').should('exist');
  });

  it('Make sure everything renders correctly', () => {
    cy.get('.navbar').should('exist');
    cy.get('[data-test=brand-name]').should('exist');
    cy.get('#navbarDarkDropdownMenuLink').should('exist');
    cy.get('[data-test=page-title]').should('exist');

    cy.get('.footer').should('exist');
    cy.get('.footer > p').contains('Designed and developed by Shakil Ahmed©2021');
  });

  it('Make sure profile menu rendered correctly', () => {
    cy.get('#navbarDarkDropdownMenuLink').click();

    cy.get('[data-test=show-profile]').should('exist');
    cy.get('[data-test=edit-profile]').should('exist');
    cy.get('[data-test=change-password]').should('exist');
    cy.get('[data-test=logout]').should('exist');

    cy.get('#navbarDarkDropdownMenuLink').click();
  });
});
